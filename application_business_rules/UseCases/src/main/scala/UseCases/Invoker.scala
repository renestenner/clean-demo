package UseCases

object Invoker {
  def invoke(command: => CommandResult): CommandResult = {
    command
  }
}
