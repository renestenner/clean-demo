package UseCases

import DataSource.{ItemsRepository, TransactionResult}
import Entities.{Character, Item}

object DefaultItemsHandler extends ItemsRepository {
  override def TransferItem(item: Item, buyer: Character, seller: Character): TransactionResult = {
    if (!seller.inventory.exists(i => i.equals(item))) {
      TransactionResult(false)
    }
    else {
      seller.inventory -= item
      buyer.inventory += item
      TransactionResult(true)
    }
  }

  override def TransferGold(gold: Int, buyer: Character, seller: Character): TransactionResult = {
    if(buyer.wallet.gold < gold) TransactionResult(false)
    else {
      buyer.wallet.gold -= gold
      seller.wallet.gold += gold
      TransactionResult(true)
    }
  }

  override def saveItemsForCharacter(items: List[Item], character: Character): TransactionResult = {
    TransactionResult(false)
  }

  override def getItemsForCharacter(character: Character): List[Item] = {
    null
  }
}
