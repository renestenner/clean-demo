package UseCases

import DataSource.ItemsRepository
import Entities.{Character, Item}



object BuyItem {
  def apply(item: Item, price: Int, buyer: Character, seller: Character, repository: ItemsRepository): CommandResult = {
    val goldResult = repository.TransferGold(price, buyer, seller)
    val itemResult = repository.TransferItem(item, buyer, seller)
    CommandResult(goldResult.wasSuccessful && itemResult.wasSuccessful)
  }
}

