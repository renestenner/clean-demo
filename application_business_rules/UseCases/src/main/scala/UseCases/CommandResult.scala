package UseCases

case class CommandResult (wasSuccessful: Boolean)
