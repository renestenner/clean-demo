package DataSource

import Entities.{Item,Character}

trait ItemsRepository {
  def TransferItem(item: Item, buyer: Character, seller: Character): TransactionResult
  def TransferGold(gold: Int, buyer: Character, seller: Character): TransactionResult
  def getItemsForCharacter(character: Character): List[Item]
  def saveItemsForCharacter(items: List[Item], character: Character): TransactionResult
}
