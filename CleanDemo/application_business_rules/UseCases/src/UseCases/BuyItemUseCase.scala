package UseCases

import DataSource.ItemsDataSource
import UseCases.Requests.{TransferItemRequest, TransferGoldRequest, BuyItemRequest}

case class BuyItemUseCase(itemsDataSource: ItemsDataSource) extends UseCase[Unit, BuyItemRequest] {
  override def execute(arg: BuyItemRequest): Unit = {
    val transferGold = TransferGoldUseCase(itemsDataSource)
    val transferGoldRequest = TransferGoldRequest(arg.item.price, arg.buyer, arg.seller)
    transferGold.execute(transferGoldRequest)

    val transferItem = TransferItemUseCase(itemsDataSource)
    val transferItemRequest = TransferItemRequest(arg.item, arg.buyer, arg.seller)
    transferItem.execute(transferItemRequest)
  }
}
